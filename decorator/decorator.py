#!/usr/bin/env python

def my_decorator(func):
  def decorated_func(*args):
    print 'decorated_func called: args=%s' % args
    func(*args)
  return decorated_func

@my_decorator
def my_func(s):
  print 'my_func called: s=%s' % s

my_func('Hello world!')
